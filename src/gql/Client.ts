import { split } from 'apollo-link';
import { HttpLink } from 'apollo-link-http';
import { WebSocketLink } from 'apollo-link-ws';
import { getMainDefinition } from 'apollo-utilities';

import ApolloClient from 'apollo-client';
import { InMemoryCache, DefaultOptions } from "apollo-boost";

import { config } from '../config/config'

const link = split(
  // split based on operation type
  ({ query }) => {
    const definition = getMainDefinition(query);
    return (
      definition.kind === 'OperationDefinition' &&
      definition.operation === 'subscription'
    );
  },
  new WebSocketLink({
    uri: config.SERVER_WS_URI,
    options: {
      reconnect: true
    }
  }),
  new HttpLink({
    uri: config.SERVER_HTTP_URI,
  }),
);

const defaultOptions: DefaultOptions = {
  watchQuery: {
    fetchPolicy: 'no-cache',
    errorPolicy: 'ignore',
  },
  query: {
    fetchPolicy: 'no-cache',
    errorPolicy: 'all',
  },
}

// cache is disabled because it didn't understood  id field
// TODO read about cache, maybe change data model
export const client = new ApolloClient({
  link: link,
  cache: new InMemoryCache({
  }),
  defaultOptions: defaultOptions,
});
