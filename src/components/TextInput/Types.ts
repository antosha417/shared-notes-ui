import { Operation, Node } from '../../Types'

export type TextInputProps = {
  addOperation: (op: Operation) => void, 
  queryOperations: (noteId: string) => void,
  subscribeToFeed: (noteId: string) => void,
  nodes: Node[], 
  maxId: number
}

export type NodesMap = {
  [operationId: string]: Node
}

export type TextInputState = {
  tree: NodesMap,
  queue: {[parentId: string]: Operation[]},
}