import React from 'react'
import { Operation, OperationId } from '../Types';
import { ROOT } from './TextInput/reducer'

function Letter(props: { operation: Operation, cursorId: OperationId, setCursorId: React.Dispatch<React.SetStateAction<OperationId>> }) {
  const { operation, cursorId, setCursorId } = props;

  return (<>
    {operation.keystroke === 'Enter' && <br />}
    {(operation.keystroke || operation.id === ROOT.operation.id) && <span
      onClick={() => setCursorId(operation.id)}
      className={(operation.id === cursorId ? "activeSpan" : "")}
    >
      {operation.keystroke === 'Enter' ? '' : operation.keystroke}
    </span>}
  </>)
}

export default Letter;