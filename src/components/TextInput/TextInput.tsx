import React, { useState, useEffect } from 'react'
import { connect } from 'react-redux'
import { useParams } from 'react-router-dom';
import './TextInput.css'

import { config } from '../../config/config'
import { Operation, Node } from '../../Types';
import { id2String } from './reducer'
import { addOperation, queryOperationsAction, publishOperationAction, subscribeToFeedAction } from './actions'
import { TextInputProps, TextInputState, NodesMap } from './Types'

import Letter from '../Letter'
import { ROOT } from './reducer';


function TextInput(props: TextInputProps) {
  // TODO figure out the type
  const noteId = (useParams() as any)?.noteId || ''
  const { maxId, nodes, addOperation, queryOperations, subscribeToFeed } = props;
  const [cursorId, setCursorId] = useState(ROOT.operation.id);

  useEffect(() => {
    subscribeToFeed(noteId);
    queryOperations(noteId);
  }, [noteId, subscribeToFeed, queryOperations])

  const onBackspace = () => {
    if (cursorId === ROOT.operation.id) return;
    const op: Operation = { id: { id: maxId + 1, uuid: config.UUID }, parentId: cursorId, delete: true, keystroke: '', noteId: noteId };
    addOperation(op);
    moveCursorLeft();
  }

  const moveCursorLeft = () => {
    setCursorId(nodes[nodes.findIndex(n => n.operation.id === cursorId) - 1]?.operation.id || ROOT.operation.id);
  }

  const moveCursorRight = () => {
    setCursorId(nodes[nodes.findIndex(n => n.operation.id === cursorId) + 1]?.operation.id || nodes[nodes.length - 1].operation.id);
  }
  

  const onKeyDown = (e: React.KeyboardEvent<HTMLDivElement>) => {
    if (e.key === 'Backspace') {
      e.preventDefault();
      return onBackspace();
    }
    if (e.key === 'ArrowLeft') {
      e.preventDefault();
      return moveCursorLeft();
    } 
    if (e.key === 'ArrowRight') {
      e.preventDefault();
      return moveCursorRight();
    }
  }

  const onKeyPress = (e: React.KeyboardEvent<HTMLDivElement>) => {
    if (e.key.length !== 1 && e.key !== 'Enter') return;
    e.preventDefault()
    const op: Operation = { id: { id: maxId + 1, uuid: config.UUID }, parentId: cursorId, keystroke: e.key, delete: false, noteId: noteId };
    addOperation(op);
    setCursorId(op.id);
  }

  return (
    <div className='text' tabIndex={0} onKeyDown={onKeyDown} onKeyPress={onKeyPress}>
      {nodes.map(n =>
        <Letter key={id2String(n.operation.id)} operation={n.operation} cursorId={cursorId} setCursorId={setCursorId} />
      )}
    </div>
  )
}

const getMaxId = (tree: NodesMap): number => Math.max(...Object.values(tree).map(n => n.operation.id.id));

const getNodes = (tree: NodesMap): Node[] => {
  const stack: Node[] = [];
  const acc: Node[] = []
  let current: Node | undefined = tree[id2String(ROOT.operation.id)]

  while (current !== undefined) {
    if (current.operation.id === ROOT.operation.id || (!current.isDeleted && !current.operation.delete)) {
      acc.push(current)
    }
    current.children.forEach(childId => stack.push(tree[childId]));
    current = stack.pop();
  }
  return acc;
}

const mapStateToProps = (state: TextInputState) => ({
  nodes: getNodes(state.tree),
  maxId: getMaxId(state.tree),
})

// TODO figure out the type of dispatch for thunk
const mapDispatchToProps = (dispatch: any) => ({
  addOperation: (operation: Operation) => { 
    dispatch(addOperation(operation)); 
    dispatch(publishOperationAction(operation));
  },
  queryOperations: (noteId: string) => dispatch(queryOperationsAction(noteId)),
  subscribeToFeed: (noteId: string) => dispatch(subscribeToFeedAction(noteId)),
})

export default connect(mapStateToProps, mapDispatchToProps)(TextInput);