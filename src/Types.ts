export type OperationId = {
    id: number,
    uuid: string,
}

export type Operation = {
    id: OperationId,
    parentId: OperationId,
    keystroke: string,
    delete: boolean,
    noteId: string,
}

export type Node = {
    operation: Operation,
    children: string[],
    isDeleted?: boolean,
}   
