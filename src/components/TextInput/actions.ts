import { Operation } from "../../Types";
import { operations, publish, subscription } from "../../gql/Queries";

export const ADD_OPERATION = 'ADD_OPERATION'
export const addOperation = (operation: Operation) => ({
  type: ADD_OPERATION,
  operation
})
export type AddOperationType = ReturnType<typeof addOperation>


export const queryOperationsAction = (noteId: string) => {
  return (dispatch: any) => {
    // console.log('quering operations for note', noteId)
    operations(noteId).then(value => {
      // console.log('operations query result', value);
      value?.data?.operations?.forEach((op: Operation) => (dispatch(addOperation(op))))
    });
  }
}

export const publishOperationAction = (op: Operation) => {
  return (dispatch: any) => {
    // console.log('publishing operation', op)
    // TODO in case of error try again
    publish(op)
  }
}

export const subscribeToFeedAction = (noteId: string) => {
  return (dispatch: any) => {
    // console.log(`subscribing with <${noteId}>`)
    subscription(noteId).forEach(value => {
      const op: Operation = value?.data?.addedOperations;
      // console.log('subscription data', value, 'operation:', op)
      if (op && op.noteId === noteId) {
        dispatch(addOperation(op))
      }
    })
  }
}

export type TextInputAction = AddOperationType