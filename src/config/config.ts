
import { v4 as uuidv4 } from 'uuid';

const getUUID = () => {
  let uuid = localStorage.getItem('uuid');
  if (!uuid) {
    uuid = uuidv4();
    localStorage.setItem('uuid', uuid);
  }
  return uuid;
}

const dev = {
  SERVER_WS_URI: 'ws://localhost:8088/ws/graphql',
  SERVER_HTTP_URI: 'http://localhost:8088/api/graphql',
  UUID: getUUID(),
}

const prod = {
  SERVER_WS_URI: 'ws://84.201.131.28:8088/ws/graphql',
  SERVER_HTTP_URI: 'http://84.201.131.28:8088/api/graphql',
  UUID: getUUID(),
}

export const config = process.env.NODE_ENV === 'development' ? dev : prod;
