import React from 'react';
import TextInput from './components/TextInput/TextInput';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';


function App() {
  return (
    <Router>
      <Switch>
        <Route
          path={'/:noteId?'}
          render={(props: Object) => <TextInput {...props} />}
        />
      </Switch>
    </Router>
  );
}

export default App;
