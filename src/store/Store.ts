import { createStore, applyMiddleware } from "redux";
import thunk from 'redux-thunk';
import { TextInputReducer } from "../components/TextInput/reducer"

// TODO figure out types
const asyncDispatchMiddleware = (store: { dispatch: (arg0: any) => void; }) => (next: (arg0: any) => void) => (action: any) => {
  let syncActivityFinished = false;
  let actionQueue: any[] = [];

  function flushQueue() {
    actionQueue.forEach(a => store.dispatch(a)); // flush queue
    actionQueue = [];
  }

  function asyncDispatch(asyncAction: any) {
    actionQueue = actionQueue.concat([asyncAction]);

    if (syncActivityFinished) {
      flushQueue();
    }
  }

  const actionWithAsyncDispatch =
      Object.assign({}, action, { asyncDispatch });

  next(actionWithAsyncDispatch);
  syncActivityFinished = true;
  flushQueue();
};   

const middlewares = [thunk, asyncDispatchMiddleware];

export const store = createStore(TextInputReducer, applyMiddleware(...middlewares))
