import { gql } from "apollo-boost";
import { client } from "./Client"
import { Operation } from "../Types"

export const ADDED_OPERATIONS_SUBSCRIPTION = gql`
  subscription($noteId: String!) {
    addedOperations(noteId: $noteId) {
      id {
        id,
        uuid
      },
      parentId {
        id,
        uuid,
      }
      keystroke,
      delete,
      noteId,
    }
  }
`;

const OPERATIONS_QUERY = gql`
  query($noteId: String!) {
    operations(noteId: $noteId) {
      id {
        id,
        uuid,
      }
      parentId {
        id, 
        uuid,
      }
      keystroke,
      delete,
      noteId,
    }
  }
`;

const ADD_OPERATION_MUTATION = gql`
  mutation (
    $id: OperationIdInput!,
    $parentId: OperationIdInput!,
    $keystroke: String!,
    $delete: Boolean!,
    $noteId: String!
  ){
    addOperation(
      id: $id,
      parentId: $parentId,
      keystroke: $keystroke,
      delete: $delete,
      noteId: $noteId
    )
  }
`;

export const publish = (op: Operation) => client.mutate({ mutation: ADD_OPERATION_MUTATION, variables: op });
export const operations = (noteId: string) => client.query({ query: OPERATIONS_QUERY, variables: {noteId: noteId} })
export const subscription = (noteId: string) => client.subscribe({ query: ADDED_OPERATIONS_SUBSCRIPTION, variables: {noteId: noteId} })  
