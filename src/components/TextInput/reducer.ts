import { ADD_OPERATION, TextInputAction, AddOperationType, addOperation, queryOperationsAction } from "./actions";
import { TextInputState } from "./Types";
import { Node, OperationId } from '../../Types';

export const id2String = (id: OperationId): string => `${id.id}__${id.uuid}`;

const PRIMARY_PROMPT_STRING = '>'
export const ROOT: Node = {
  operation: {
    id: { id: 0, uuid: '0' },
    parentId: { id: 0, uuid: '0' },
    keystroke: PRIMARY_PROMPT_STRING,
    delete: false,
    noteId: ''
  },
  children: []
}

const initialState: TextInputState = {
  tree: { [id2String(ROOT.operation.id)]: ROOT },
  queue: {}
}

export const TextInputReducer = (state: TextInputState = initialState, action: TextInputAction): TextInputState => {
  if (action.type === ADD_OPERATION) {
    const { tree, queue } = state;
    const operation = (action as AddOperationType).operation;
    // console.log('incerting operation', operation)
    const id = id2String(operation.id)
    if (tree[id]) {
      return state;
    }
    const parentId = id2String(operation.parentId)
    const parent = tree[parentId]

    if (!parent) {
      // parent is not inserted yet
      // adding to queue
      // console.warn('adding to queue', operation);
      // TODO is it ok to request all hisotry?
      // TODO add ocasional request of all ops from server 
      // console.info('requesting all history from server');
      (action as any).asyncDispatch(queryOperationsAction(operation.noteId));
      const orphans = queue[parentId] || []
      return {...state, queue: {...queue, [parentId]: [...orphans, operation]}}
    }

    if (operation.delete && parent !== ROOT) {
      // console.log('deleting parent', parent);
      return {...state,
        tree: {
          ...tree,
          [parentId]: {...parent, isDeleted: true},
        }
      }
    }

    // TODO children already sorted can insert in O(n) instead of O(nlogn)
    const parentChildren = [...parent.children, id].sort((aId, bId) => {
      const a = aId === id ? operation : tree[aId].operation;
      const b = bId === id ? operation : tree[bId].operation;
      if (a.id.id > b.id.id) {
        return 1;
      } else if (a.id.id < b.id.id) {
        return -1;
      } else {
        return a.id.uuid > b.id.uuid ? 1 : -1;
      }
    })

    // check queue for operations waiting for this `operation`
    // TODO figure out types
    queue[id]?.forEach(op => (action as any).asyncDispatch(addOperation(op)))

    return { ...state, 
      tree: { 
        ...tree, 
        [id]: { operation, children: [] }, 
        [parentId]: { ...parent, children: parentChildren } 
      }, 
      queue: {...queue, id: []} };
  }
  return state;
}